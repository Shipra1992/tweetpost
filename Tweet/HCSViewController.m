//
//  HCSViewController.m
//  Tweet
//
//  Created by Shipra Gupta on 09/10/14.
//  Copyright (c) 2014 Hot Cocoa Software Pvt. Ltd. All rights reserved.
//


//Use of UIActivityViewController

#import "HCSViewController.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import <Accounts/Accounts.h>
#import <Social/Social.h>

@interface HCSViewController () <UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextViewDelegate, UIAlertViewDelegate>

@property (nonatomic, weak) IBOutlet UIImageView *imageView;
@property (nonatomic, weak) IBOutlet UITextView *descriptionTextView;
@property (nonatomic, strong) UIToolbar *accessoryToolBar;
@property (nonatomic, strong) ACAccountStore *accountStore;
@property (nonatomic, strong) ACAccount *twitterAccount;
@property (nonatomic, strong) ACAccount *facebookAccount;
@property (nonatomic, strong) NSString *action;

@end

@implementation HCSViewController

#pragma mark - View Life Cycle Methods

- (void)viewDidLoad {
    [super viewDidLoad];
    self.descriptionTextView.delegate = self;
    self.descriptionTextView.autocorrectionType = UITextAutocorrectionTypeNo;
    [[UITextView appearance] setTintColor:[UIColor grayColor]];
    [self setupAccessoryView];
}

#pragma mark - actions

- (IBAction)post:(id)sender {
    NSArray *activityItems = [NSArray arrayWithObjects:self.descriptionTextView.text, self.imageView.image, nil];
    
    UIActivityViewController *activityViewController = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
    activityViewController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    activityViewController.excludedActivityTypes =  @[UIActivityTypePrint, UIActivityTypeCopyToPasteboard, UIActivityTypeAssignToContact, UIActivityTypeSaveToCameraRoll];
    
    [self presentViewController:activityViewController animated:YES completion:nil];
    
    [activityViewController setCompletionHandler:^(NSString *act, BOOL done) {
        NSString *alertMessage = nil;
        self.action = act;
        if ([act isEqualToString:UIActivityTypePostToTwitter])  alertMessage = @"Posted on Twitter";
        if ([act isEqualToString:UIActivityTypePostToFacebook]) alertMessage = @"Posted on Facebook";
        if (done) {
            [[[UIAlertView alloc] initWithTitle:alertMessage message:@"" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            self.imageView.image = nil;
            self.descriptionTextView.text = @"";
        }
    }];
}

- (IBAction)takePhoto:(id)sender {
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc]init];
    imagePickerController.delegate = self;
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
    } else {
        imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    }
    imagePickerController.allowsEditing = YES;
    [self presentViewController:imagePickerController animated:YES completion:NULL];
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        [self postDidHappen];
    }
}

#pragma mark - Helper Method

- (void)postDidHappen {
    self.accountStore = [[ACAccountStore alloc] init];
    
    if ([self.action isEqualToString:UIActivityTypePostToTwitter]) {
    ACAccountType *twitterAccount = [self.accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
    
    [self.accountStore requestAccessToAccountsWithType:twitterAccount options:nil completion:^(BOOL granted, NSError *error) {
         if (granted) {
             self.twitterAccount = [[self.accountStore accountsWithAccountType:twitterAccount] lastObject];
             NSLog(@"Twitter UserName: %@", self.twitterAccount.username);
             
             NSString *urlString = [NSString stringWithFormat:@"twitter://user?screen_name=%@",self.twitterAccount.username];
             NSString *twitterString = [NSString stringWithFormat:@"http://www.twitter.com/%@", self.twitterAccount.username];
             NSURL *twitterURL = [NSURL URLWithString:urlString];
             if ([[UIApplication sharedApplication] canOpenURL:twitterURL])
                 [[UIApplication sharedApplication] openURL:twitterURL];
             else
                 [[UIApplication sharedApplication] openURL:[NSURL URLWithString:twitterString]];
         } else {
             if (error == nil) {
//                 NSLog(@"User Has disabled your app from settings...");
             } else {
                 NSLog(@"Error in Login: %@", error);
             }
         }
     }];
    } else if ([self.action isEqualToString:UIActivityTypePostToFacebook]) {
        //facebook login
        [[UIApplication sharedApplication] openURL:
         [NSURL URLWithString: @"https://facebook.com/myAppsPage"]];
    }
}

#pragma mark - Accessory View

- (void)setupAccessoryView {
    self.accessoryToolBar = [[UIToolbar alloc] init];
    self.accessoryToolBar.barStyle = UIBarStyleDefault ;
    self.accessoryToolBar.items = [NSArray arrayWithObjects:[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil], [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleBordered target:self action:@selector(dismissKeyboard:)], nil];
    [self.accessoryToolBar sizeToFit];
    self.descriptionTextView.inputAccessoryView = self.accessoryToolBar;
}

- (void)dismissKeyboard:(id)sender {
    [self.descriptionTextView resignFirstResponder];
}


#pragma marks - UIImagePickerControllerDelegate

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *image = info[UIImagePickerControllerEditedImage];
    if (!image) {
        image = info[UIImagePickerControllerOriginalImage];
    }
    self.imageView.image = image;
    [self dismissViewControllerAnimated:YES completion:NULL];
}

@end
