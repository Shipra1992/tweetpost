//
//  HCSAppDelegate.h
//  Tweet
//
//  Created by Shipra Gupta on 09/10/14.
//  Copyright (c) 2014 Hot Cocoa Software Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HCSAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
